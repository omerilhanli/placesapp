package com.ilhanli.omer.placesapp.UI;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;

import butterknife.ButterKnife;

/**
 * Created by omerilhanli on 7.07.2018.
 */

public abstract class BaseActivity extends AppCompatActivity {


    protected abstract int contentViewId();

    protected abstract String getToolbarTitle();

    protected abstract boolean getShowHomeButton();


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(contentViewId());

        ButterKnife.bind(this);
    }
}
