package com.ilhanli.omer.placesapp.object.tips;

import com.ilhanli.omer.placesapp.object.explore.Meta;

import java.io.Serializable;

import javax.annotation.Generated;

/**
 * Created by omerilhanli on 8.07.2018.
 */

@Generated("org.jsonschema2pojo")
public class VenueTips implements Serializable{

    private static final long serialVersionUID = 5975452450711339585L;

    private Meta meta;

    private Response response;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
