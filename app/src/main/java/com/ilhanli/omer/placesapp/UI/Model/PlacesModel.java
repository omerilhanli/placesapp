package com.ilhanli.omer.placesapp.UI.Model;

import android.os.Bundle;

import com.ilhanli.omer.placesapp.UI.Contract.IPlacesView;
import com.ilhanli.omer.placesapp.bundle.BundleAttribute;
import com.ilhanli.omer.placesapp.interfaces.OnExploreComplete;
import com.ilhanli.omer.placesapp.object.PlaceItem;
import com.ilhanli.omer.placesapp.object.explore.Venue;
import com.ilhanli.omer.placesapp.service.FourSquareService;
import com.ilhanli.omer.placesapp.utility.Util;

import java.util.List;

public class PlacesModel implements IPlacesView.IModel {

    private IPlacesView.IPresenterForModel presenter;

    public PlacesModel(IPlacesView.IPresenterForModel presenter) {

        this.presenter = presenter;
    }

    @Override
    public void mInit(Bundle bundle) {

        // Liste ve Query bilgisi intent yardımıyla MainAktivityden alınır, PlacesActivityde kullanılır.
        if (bundle != null) {

            String query = bundle.getString(BundleAttribute.BUNDLE_QUERY);

            List<PlaceItem> placeList = (List<PlaceItem>) bundle.getSerializable(BundleAttribute.BUNDLE_PLACE_LIST);

            // Alınan datalar ile View güncellemek üzere presenter notify edilir.
            presenter.pmNotify(query, placeList);
        }
    }

    // Listedeki her item click eventiyle search işlemi gerçekleşir.
    @Override
    public void mItemSearch(final FourSquareService fourSquareService,final PlaceItem placeItem, final String query) {

        final Venue venue = placeItem.getVenue();

        // Mekan resim ve yorum bilgisi için mekan id alınır
        final String venueId = venue.getId();

        double lat = venue.getLocation().getLat();

        double lng = venue.getLocation().getLng();

        // Mekan resim ve yorum bilgisi için adres alınır
        final String address = Util.findAdressSummary(presenter.pmGetContext(), lat, lng);

        // İlk önce resim requesti atılır. Resim urli alınır, url null gelse de devamında yorum requesti atılır
        // Günlük belli bir request limiti var, 24 saat dolduğunda bu limit sıfırlanır. Limit aşıldığında
        // quota_exceeded error mesajı response olarak alınır.
        fourSquareService.requestVenuePhotos(venueId, address, query,

                new OnExploreComplete() {

                    @Override
                    public void onExploreCompleted(List<PlaceItem> placeItemList) {
                    }

                    @Override
                    public void onVenuePhotosCompleted(final String venuePhotoUrl) {

                        fourSquareService.requestVenueTips(venueId, address, query,

                                new OnExploreComplete() {

                                    @Override
                                    public void onExploreCompleted(List<PlaceItem> placeItemList) {

                                    }

                                    @Override
                                    public void onVenuePhotosCompleted(String photoUrl) {

                                    }

                                    @Override
                                    public void onVenueTipsCompleted(String venueTip) {

                                        // sonuç olarak View güncellenmek üzere, presenter notify edilir.
                                        presenter.pmFinished(placeItem, venueTip, venuePhotoUrl);
                                    }
                                });
                    }

                    @Override
                    public void onVenueTipsCompleted(String venueTip) {
                    }
                });
    }
}
