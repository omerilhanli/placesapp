package com.ilhanli.omer.placesapp.UI.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.ilhanli.omer.placesapp.UI.Contract.IPlacesView;
import com.ilhanli.omer.placesapp.UI.NormalBaseActivity;
import com.ilhanli.omer.placesapp.UI.Presenter.PlacesPresenter;
import com.ilhanli.omer.placesapp.bundle.BundleAttribute;
import com.ilhanli.omer.placesapp.UI.Adapter.PlacesRecyclerAdapter;
import com.ilhanli.omer.placesapp.R;
import com.ilhanli.omer.placesapp.interfaces.OnAdapterListener;
import com.ilhanli.omer.placesapp.object.PlaceItem;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;

public class PlacesActivity extends NormalBaseActivity implements OnAdapterListener, IPlacesView.IView {

    @BindView(R.id.recycler_view_places)
    RecyclerView recyclerViewPlaces;

    private IPlacesView.IPresenter presenter;

    private String mQuery;

    // Static olarak activity başlatılır, Intent yardımıyla Mekan ve Liste bundle'a eklenir.
    public static void startActivity(Activity activity, String query, List<PlaceItem> placeList) {

        Intent intent = new Intent(activity, PlacesActivity.class);

        intent.putExtra(BundleAttribute.BUNDLE_QUERY, query);

        intent.putExtra(BundleAttribute.BUNDLE_PLACE_LIST, (Serializable) placeList);

        activity.startActivity(intent);
    }

    @Override
    protected int contentViewId() {

        return R.layout.activity_places;
    }

    @Override
    protected String getToolbarTitle() {

        return getString(R.string.title_bar_places);
    }

    @Override
    protected boolean getShowHomeButton() {

        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Activity, aslında view implementation'ıdır, ve Presenter'dan bir obje barındırı
        // Bu objeyi oluşturur ve pInit ile başlatır.
        presenter = new PlacesPresenter(this);

        presenter.pInit((PlacesPresenter) presenter, getIntent().getExtras());

    }

    // Toolbar üzerindeki geri tuşu basıldığında önceki activity'ye gidiş sağlanır
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case android.R.id.home:

                onBackPressed();

                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    // Listedeki her item click eventinin düştüğü listener
    @Override
    public void onTapItemListen(final PlaceItem placeItem) {

        presenter.pItemSearch(fourSquareService, placeItem, mQuery);
    }

    @Override
    public void vInit(String query, List<PlaceItem> placeItemList) {

        this.mQuery = query;

        // PlacesActivity ilk yüklenidiğinde Bundle ile gelen liste ile güncellenir.
        prepareRecycler(placeItemList);
    }

    @Override
    public Context vGetContext() {

        return this;
    }

    @Override
    public void vShowProgress() {

        showDialog();
    }

    @Override
    public void vHideProgress() {

        dismissDialog();
    }


    public void prepareRecycler(List<PlaceItem> placeList) {

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);

        PlacesRecyclerAdapter placesRecyclerAdapter = new PlacesRecyclerAdapter(this, placeList);


        recyclerViewPlaces.setLayoutManager(manager);

        recyclerViewPlaces.addItemDecoration(dividerItemDecoration);

        recyclerViewPlaces.setAdapter(placesRecyclerAdapter);
    }
}
