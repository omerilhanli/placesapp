package com.ilhanli.omer.placesapp.service;

import android.util.Log;

import com.ilhanli.omer.placesapp.interfaces.OnExploreComplete;
import com.ilhanli.omer.placesapp.object.PlaceItem;
import com.ilhanli.omer.placesapp.object.explore.Explore;
import com.ilhanli.omer.placesapp.object.photos.VenuePhoto;
import com.ilhanli.omer.placesapp.object.tips.VenueTips;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FourSquareServiceImpl implements FourSquareService {

    public static final String CLIENT_ID = "VIEQ0QX5GAJ1XLDJABA5WBS54XCVTNWLNY2NLAZVNB2ZDUYM";

    public static final String CLIENT_SECRET = "COARL4531NXUEZTWDE21201TRAZXPEFIQKXFY4AJKHWHDXOT";

    public static final String API_VERSION = "20180323";

    private FourSquareApi fourSquareApi;

    public FourSquareServiceImpl() {

        fourSquareApi = FourSquareApi.Creator.create(FourSquareApi.class);
    }


    /**
     * Aranan venue için yorum bulma servis metodu
     *
     * @param venueId
     * @param address
     * @param query
     * @param onExploreComplete
     */
    public void requestVenueTips(String venueId, String address, String query, final OnExploreComplete onExploreComplete) {

        Call<VenueTips> call = fourSquareApi.requestVenueTips(
                venueId,
                CLIENT_ID,
                CLIENT_SECRET,
                API_VERSION,
                address,
                query);

        call.enqueue(new Callback<VenueTips>() {
            @Override
            public void onResponse(Call<VenueTips> call, Response<VenueTips> response) {

                VenueTips venueTips = response.body();

                String tip;

                if (venueTips != null && venueTips.getResponse().getTips() != null
                        && venueTips.getResponse().getTips().getItems() != null
                        && venueTips.getResponse().getTips().getItems().size() != 0
                        && venueTips.getResponse().getTips().getItems().get(0) != null) {

                    tip = venueTips.getResponse().getTips().getItems().get(0).getText();

                    if (tip == null || tip.isEmpty()) {

                        tip = "Mekan hakkında yorum bulunamadı!";
                    }

                } else {

                    tip = "Mekan hakkında yorum bulunamadı!";
                }

                onExploreComplete.onVenueTipsCompleted(tip);
            }

            @Override
            public void onFailure(Call<VenueTips> call, Throwable t) {

                Log.e("ERROR", t.getMessage());

                onExploreComplete.onVenueTipsCompleted(null);
            }
        });
    }

    /**
     *  Aranan venue fotoğrafı bulma servis metodu
     *
     * @param venueId
     * @param address
     * @param query
     * @param onExploreComplete
     */
    public void requestVenuePhotos(String venueId, String address, String query,
                                   final OnExploreComplete onExploreComplete) {

        Call<VenuePhoto> call = fourSquareApi.requestVenuePhoto(
                venueId,
                CLIENT_ID,
                CLIENT_SECRET,
                API_VERSION,
                address,
                query);

        call.enqueue(new Callback<VenuePhoto>() {
            @Override
            public void onResponse(Call<VenuePhoto> call, Response<VenuePhoto> response) {

                VenuePhoto venuePhoto = response.body();

                String size = "512x512";

                String url = null;

                if (venuePhoto != null &&
                        venuePhoto.getResponse() != null
                        && venuePhoto.getResponse().getPhotos() != null
                        && venuePhoto.getResponse().getPhotos().getItems() != null
                        && venuePhoto.getResponse().getPhotos().getItems().size() != 0
                        && venuePhoto.getResponse().getPhotos().getItems().get(0) != null) {

                    String prefix = venuePhoto.getResponse().getPhotos().getItems().get(0).getPrefix();

                    String suffix = venuePhoto.getResponse().getPhotos().getItems().get(0).getSuffix();

                    url = prefix + size + suffix;
                }

                onExploreComplete.onVenuePhotosCompleted(url);
            }

            @Override
            public void onFailure(Call<VenuePhoto> call, Throwable t) {

                Log.e("ERROR", t.getMessage());

                onExploreComplete.onVenuePhotosCompleted(null);
            }
        });
    }


    /**
     * İstenen adreste (veya mevcut konum adresinde) ve istenen mekan tipinde
     * Mekanlar bulma servis metodu
     *
     * @param address
     * @param query
     * @param onExploreComplete
     */
    public void requestExplore(String address, String query,
                               final OnExploreComplete onExploreComplete) {

        Call<Explore> call = fourSquareApi.requestExplore(
                CLIENT_ID,
                CLIENT_SECRET,
                API_VERSION,
                address,
                query);

        call.enqueue(new Callback<Explore>() {
            @Override
            public void onResponse(Call<Explore> call, Response<Explore> response) {

                List<PlaceItem> placeItemList = null;
                try {

                    if (response != null && response.body() != null) {

                        Explore explore = response.body();

                        if (explore != null && explore.getResponse() != null
                                && explore.getResponse().getGroups() != null
                                && explore.getResponse().getGroups().size() != 0
                                && explore.getResponse().getGroups().get(0) != null
                                && explore.getResponse().getGroups().get(0).getItems() != null)

                            placeItemList = explore.getResponse().getGroups().get(0).getItems();
                    }

                    onExploreComplete.onExploreCompleted(placeItemList);

                } catch (Exception e) {

                    onExploreComplete.onExploreCompleted(null);

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<Explore> call, Throwable t) {

                Log.e("ERROR", t.getMessage());

                onExploreComplete.onExploreCompleted(null);
            }
        });
    }
}
