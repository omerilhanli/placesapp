package com.ilhanli.omer.placesapp.interfaces;

import com.ilhanli.omer.placesapp.object.PlaceItem;

/**
 * PlacesRecyclerAdapter item click eventi için listener.
 * Her item click eventi PlacesActivity'ye aktarılır.
 */
public interface OnAdapterListener {

    void onTapItemListen(PlaceItem placeItem);
}
