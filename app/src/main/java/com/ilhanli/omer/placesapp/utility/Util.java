package com.ilhanli.omer.placesapp.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilhanli.omer.placesapp.R;
import com.ilhanli.omer.placesapp.object.explore.Location;
import com.ilhanli.omer.placesapp.object.PlaceItem;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

/**
 * Created by omerilhanli on 7.07.2018.
 */

public class Util {

    // Splash ekranının full screen açılması
    public static void getFullScreen(Activity activity) {

        View decorView = activity.getWindow().getDecorView();

        decorView.setSystemUiVisibility(

                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY

                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        );
    }

    // Karakter validationından geçemeyen kontrollerde popup uyarısı
    public static void showPopup(Context c, String title, String message,
                                 DialogInterface.OnClickListener okButton) {
        AlertDialog.Builder adBuilder = new AlertDialog.Builder(c);
        adBuilder.setMessage(message);
        adBuilder.setTitle(title);
        adBuilder.setCancelable(true);
        adBuilder.setPositiveButton(android.R.string.ok, okButton);
        AlertDialog alertDialogObj = adBuilder.create();
        alertDialogObj.show();
    }

    // Custom bir view ile dialog oluştururuz
    public static void showDialog(Context c, PlaceItem placeItem, String venueTip, String venuePhotoUrl) {

        AlertDialog.Builder adBuilder = new AlertDialog.Builder(c);

        View view = LayoutInflater.from(c).inflate(R.layout.dialog_place_detail, null);

        // create
        ImageView imageViewMap = view.findViewById(R.id.image_view_map_picture);

        ImageView imageViewPlace = view.findViewById(R.id.image_view_place_picture);

        TextView textViewPlaceName = view.findViewById(R.id.text_view_place_name);

        TextView textViewComment = view.findViewById(R.id.text_view_comment);

        // load
        // Google Statik Map api ile, adresin haritadaki konum resmi alınır.
        String mapUrl = getVenueMapPhotoUrl(placeItem.getVenue().getLocation());

        if (mapUrl != null && !mapUrl.isEmpty()) {

            Picasso.get().load(mapUrl).into(imageViewMap);

        } else {

            // Eğer alınamazsa default uygulama ikonu yüklenir.
            Picasso.get().load(R.drawable.playstore_icon).into(imageViewMap);
        }

        if (venuePhotoUrl != null && !venuePhotoUrl.isEmpty()) {

            Picasso.get().load(venuePhotoUrl).into(imageViewPlace);

        } else {

            // Eğer alınamazsa default uygulama ikonu yüklenir.
            Picasso.get().load(R.drawable.playstore_icon).into(imageViewPlace);
        }

        if (placeItem != null && placeItem.getVenue() != null && placeItem.getVenue().getName() != null) {

            textViewPlaceName.setText(placeItem.getVenue().getName());
        }

        if (venueTip != null && !venueTip.isEmpty()) {

            textViewComment.setText(venueTip);
        }


        // prepare alert
        adBuilder.setView(view);

        adBuilder.setCancelable(true);

        final AlertDialog adFinal = adBuilder.create();

        adFinal.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        adFinal.show();
    }


    public static String getVenueMapPhotoUrl(Location location) {

        double latitude = location.getLat();

        double longtitude = location.getLng();

        String mapPhotoUrl = "http://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longtitude +
                "&markers=" + latitude + "," + longtitude +
                "&size=512x512&sensor=false&zoom=16";

        return mapPhotoUrl;
    }

    // Location bilgisinden tam adresi almamızı sağlar.
    public static String findAdressFull(Context context, Location location) {

        double latitude = location.getLat();

        double longitude = location.getLng();

        String searchedAddress = "";

        try {

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());

            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

            searchedAddress = addresses.get(0).getAddressLine(0);

            String country = addresses.get(0).getCountryName();

            int offsetAddress = searchedAddress.indexOf(country);

            searchedAddress = searchedAddress.substring(0, offsetAddress - 2).trim();


        } catch (Exception e) {

            e.printStackTrace();
        }

        return searchedAddress;
    }

    // Lokasyon bilgisinden kısa adres almamızı sağlar
    public static String findAdressSummary(Context context, double latitude, double longitude) {

        String searchedAddress = "";

        try {

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());

            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);

            String subAdminArea = addresses.get(0).getSubAdminArea();

            String adminArea = addresses.get(0).getAdminArea();

            searchedAddress = subAdminArea + "/" + adminArea;

            System.out.println("searchedAddress:" + searchedAddress);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return searchedAddress;
    }
}
