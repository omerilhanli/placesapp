package com.ilhanli.omer.placesapp.object.photos;

import com.ilhanli.omer.placesapp.object.explore.Photo;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

/**
 * Created by omerilhanli on 8.07.2018.
 */
@Generated("org.jsonschema2pojo")
public class Response implements Serializable{

    private static final long serialVersionUID = -5364213361509318306L;

    private Photos photos;

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }
}
