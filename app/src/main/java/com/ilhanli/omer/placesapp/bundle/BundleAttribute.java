package com.ilhanli.omer.placesapp.bundle;

/**
 * Created by omerilhanli on 7.07.2018.
 */

public class BundleAttribute {

    public static final String BUNDLE_QUERY = "Query";

    public static final String BUNDLE_PLACE_LIST = "PlaceList";

}
