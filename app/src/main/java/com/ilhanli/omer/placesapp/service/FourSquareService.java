package com.ilhanli.omer.placesapp.service;

import com.ilhanli.omer.placesapp.interfaces.OnExploreComplete;

public interface FourSquareService {

    void requestVenueTips(String venueId, String address, String query,
                          final OnExploreComplete onExploreComplete);

    void requestVenuePhotos(String venueId, String address, String query,
                            final OnExploreComplete onExploreComplete);

    void requestExplore(String address, String query,
                   final OnExploreComplete onExploreComplete);

}
