package com.ilhanli.omer.placesapp.component;

import com.ilhanli.omer.placesapp.UI.NormalBaseActivity;
import com.ilhanli.omer.placesapp.module.MyModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = MyModule.class)
public interface MyComponent {

    // NormalBaseActivity'ye objemizi inject ederiz.
    void inject(NormalBaseActivity normalBaseActivity);
}
