package com.ilhanli.omer.placesapp;

import android.app.Application;

import com.ilhanli.omer.placesapp.component.DaggerMyComponent;
import com.ilhanli.omer.placesapp.component.MyComponent;
import com.ilhanli.omer.placesapp.module.MyModule;

/**
 * Created by omerilhanli on 7.07.2018.
 */

public class MyApplication extends Application {

    private MyComponent mMyComponent;

    @Override
    public void onCreate() {

        super.onCreate();

        mMyComponent = createMyComponent();
    }

   public MyComponent getMyComponent() {
        return mMyComponent;
    }

    private MyComponent createMyComponent() {

        return DaggerMyComponent
                .builder()
                .myModule(new MyModule())
                .build();
    }
}