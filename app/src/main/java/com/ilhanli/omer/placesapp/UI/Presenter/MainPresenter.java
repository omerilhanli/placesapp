package com.ilhanli.omer.placesapp.UI.Presenter;

import android.app.Activity;
import android.content.Context;

import com.ilhanli.omer.placesapp.UI.Contract.IMainView;
import com.ilhanli.omer.placesapp.UI.Model.MainModel;
import com.ilhanli.omer.placesapp.UI.View.PlacesActivity;
import com.ilhanli.omer.placesapp.object.PlaceItem;
import com.ilhanli.omer.placesapp.service.FourSquareService;

import java.util.List;

public class MainPresenter implements IMainView.IPresenter, IMainView.IPresenterForModel {

    private IMainView.IView mView;

    private IMainView.IModel mModel;

    public MainPresenter(IMainView.IView mView) {

        this.mView = mView;
    }

    @Override
    public void pInit(MainPresenter mainPresenter) {

        // Presenter model objesini alır ve vInit methodunu çağırır.
        mModel = new MainModel(mainPresenter);

        mModel.mInit();
    }

    // Viewden alınan search isteği üzerine modele search işlemi paslanır
    @Override
    public void pEventSearch(FourSquareService fourSquareService,String query, String city) {

        mView.vShowProgress();

        mModel.mEventSearch(fourSquareService,query, city);
    }

    // Model search işlemi sonuçlandığında Presenter, Viewi günceller
    @Override
    public void pmFinished(List<PlaceItem> placeItemList, String query) {

        mView.vHideProgress();

        if (placeItemList != null) {

            PlacesActivity.startActivity((Activity) pmGetContext(), query, placeItemList);
        }
    }

    @Override
    public Context pmGetContext() {

        return mView.vGetContext();
    }
}
