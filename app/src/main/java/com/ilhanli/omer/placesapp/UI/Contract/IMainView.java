package com.ilhanli.omer.placesapp.UI.Contract;

import android.content.Context;

import com.ilhanli.omer.placesapp.UI.Presenter.MainPresenter;
import com.ilhanli.omer.placesapp.object.PlaceItem;
import com.ilhanli.omer.placesapp.service.FourSquareService;

import java.util.List;

/**
 * MVP pattern'i için Model, View ve Presenter için ayrı ayrı interface'ler yazılır
 * Bir de Presenter ve Model arasındaki iletişimi sağlayan PresenterForModel interface'i eklenir.
 */
public interface IMainView {

    interface IModel {

        void mInit();

        // Search işlemi, Model içinde gerçekleşir
        void mEventSearch(FourSquareService fourSquareService,String query, String city);
    }

    interface IView {

        Context vGetContext();

        void vShowProgress();

        void vHideProgress();
    }

    interface IPresenter {

        void pInit(MainPresenter mainPresenter);

        // View'dan modele paslamak üzere datalar alınır
        void pEventSearch(FourSquareService fourSquareService,String query, String city);
    }

    interface IPresenterForModel {

        Context pmGetContext();

        // Search işlemi tamamlandığında, View güncellenmek üzere Model presenter'i notify eder.
        void pmFinished(List<PlaceItem> placeItemList, String query);
    }
}
