package com.ilhanli.omer.placesapp.UI.Presenter;

import android.content.Context;
import android.os.Bundle;

import com.ilhanli.omer.placesapp.UI.Contract.IPlacesView;
import com.ilhanli.omer.placesapp.UI.Model.PlacesModel;
import com.ilhanli.omer.placesapp.object.PlaceItem;
import com.ilhanli.omer.placesapp.service.FourSquareService;
import com.ilhanli.omer.placesapp.utility.Util;

import java.util.List;

public class PlacesPresenter implements IPlacesView.IPresenter, IPlacesView.IPresenterForModel {

    private IPlacesView.IView mView;

    private IPlacesView.IModel mModel;

    public PlacesPresenter(IPlacesView.IView mView) {

        this.mView = mView;
    }

    @Override
    public void pInit(PlacesPresenter presenter, Bundle bundle) {

        // Presenter model objesini alır ve vInit methodunu çağırır.
        mModel = new PlacesModel(presenter);

        mModel.mInit(bundle);
    }

    @Override
    public void pItemSearch(FourSquareService fourSquareService, PlaceItem placeItem, String query) {

        mView.vShowProgress();

        // Listedeki her item click eventi ile search işlemi için Modelin mItemSearch servisi çağırılır
        mModel.mItemSearch(fourSquareService,placeItem, query);
    }

    // Item search işlemi sonuçlanınca presenter modeli günceller.
    @Override
    public void pmFinished(PlaceItem placeItem, String venueTip, String venuePhotoUrl) {

        mView.vHideProgress();

        Util.showDialog(pmGetContext(), placeItem, venueTip, venuePhotoUrl);
    }

    @Override
    public Context pmGetContext() {

        return mView.vGetContext();
    }

    // İlk yüklemede Model, Intent ile gelen mekan ve mekanlar liste verisini alır ve View güncellenir.
    @Override
    public void pmNotify(String query, List<PlaceItem> placeItemList) {

        mView.vInit(query, placeItemList);
    }
}
