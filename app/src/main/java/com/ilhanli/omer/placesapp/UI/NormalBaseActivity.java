package com.ilhanli.omer.placesapp.UI;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import com.ilhanli.omer.placesapp.MyApplication;
import com.ilhanli.omer.placesapp.R;
import com.ilhanli.omer.placesapp.service.FourSquareService;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by omerilhanli on 8.07.2018.
 */

public abstract class NormalBaseActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    protected View fullLayoutView;

    protected ViewGroup contentViGoo;

    public ProgressDialog progressDialog;

    // Dagger2 container ile obje inject edilir
    @Inject
    public FourSquareService fourSquareService;

    // her activitynin layout idsi çekilir
    protected abstract int contentViewId();

    protected abstract String getToolbarTitle();

    // Toolbar üzerinde geri tuşu
    protected abstract boolean getShowHomeButton();

    // setContentView override ile container olarak ayarlanan layout içerisine, her activity yüklemesi sağlanır
    @Override
    public void setContentView(@LayoutRes int layoutResID) {

        fullLayoutView = getLayoutInflater().inflate(R.layout.normal_base_layout, null);

        contentViGoo = (ViewGroup) fullLayoutView.findViewById(R.id.frame_layout_content);

        getLayoutInflater().inflate(contentViewId(), contentViGoo, true);

        super.setContentView(fullLayoutView);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // injection initilization
        ((MyApplication) getApplication())
                .getMyComponent()
                .inject(this);

        progressDialog = new ProgressDialog(this);

        progressDialog.setMessage("Loading...");

        progressDialog.setCancelable(true);


        configToolbar();
    }

    public void configToolbar() {

        setSupportActionBar(toolbar);

        toolbar.setTitle(getToolbarTitle());

        toolbar.setTitleTextColor(Color.BLACK);

        getSupportActionBar().setDisplayHomeAsUpEnabled(getShowHomeButton());
    }

    public void showDialog() {

        if (progressDialog != null && !progressDialog.isShowing()) {

            progressDialog.show();
        }
    }

    public void dismissDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {

            progressDialog.dismiss();
        }
    }
}
