package com.ilhanli.omer.placesapp.UI.Contract;

import android.content.Context;
import android.os.Bundle;

import com.ilhanli.omer.placesapp.UI.Presenter.PlacesPresenter;
import com.ilhanli.omer.placesapp.object.PlaceItem;
import com.ilhanli.omer.placesapp.service.FourSquareService;

import java.util.List;

/**
 * MVP pattern'i için Model, View ve Presenter için ayrı ayrı interface'ler yazılır
 * Bir de Presenter ve Model arasındaki iletişimi sağlayan PresenterForModel interface'i eklenir.
 */
public interface IPlacesView {

    interface IModel {

        void mInit(Bundle bundle);

        // Listedeki her item için, Adapter gelen her click eventiyle search işlemi gerçekleşir.
        void mItemSearch(FourSquareService fourSquareService,final PlaceItem placeItem, String query);
    }

    interface IView {

        void vInit(String query, List<PlaceItem> placeItemList);

        Context vGetContext();

        void vShowProgress();

        void vHideProgress();
    }

    interface IPresenter {

        void pInit(PlacesPresenter presenter, Bundle bundle);

        // View den gelen search isteğini modele aktarmak üzere çağırılır
        void pItemSearch(FourSquareService fourSquareService,final PlaceItem placeItem, String query);
    }

    interface IPresenterForModel {

        Context pmGetContext();

        // View, ilk yüklemede güncellenmek üzere, model presenteri notify eder.
        void pmNotify(String query, List<PlaceItem> placeItemList);

        // item search işlemi tamamlandığında presenter notify edilir.
        void pmFinished(PlaceItem placeItem, String venueTip, String venuePhotoUrl);
    }
}
