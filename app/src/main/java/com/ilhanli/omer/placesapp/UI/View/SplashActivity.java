package com.ilhanli.omer.placesapp.UI.View;

import android.os.Handler;
import android.os.Bundle;

import com.ilhanli.omer.placesapp.UI.BaseActivity;
import com.ilhanli.omer.placesapp.R;
import com.ilhanli.omer.placesapp.utility.Util;

public class SplashActivity extends BaseActivity {

    public static final int SPLASH_DELAY = 2000;


    @Override
    protected int contentViewId() {

        return R.layout.activity_splash;
    }

    @Override
    protected String getToolbarTitle() {
        return null;
    }

    @Override
    protected boolean getShowHomeButton() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Splash Screen (Activity) 2 sn açık kalır ve bir Handler yardımıyla MainActivity ye geçilir.
        Util.getFullScreen(this);

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


                MainActivity.startActivity(SplashActivity.this);

            }

        }, SPLASH_DELAY);
    }

}
