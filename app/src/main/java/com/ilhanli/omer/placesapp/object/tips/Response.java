package com.ilhanli.omer.placesapp.object.tips;

import com.ilhanli.omer.placesapp.object.photos.Photos;

import java.io.Serializable;

import javax.annotation.Generated;

/**
 * Created by omerilhanli on 8.07.2018.
 */
@Generated("org.jsonschema2pojo")
public class Response implements Serializable{

    private static final long serialVersionUID = 7663518125770054910L;
    private Tips tips;

    public Tips getTips() {
        return tips;
    }

    public void setTips(Tips tips) {
        this.tips = tips;
    }
}
