package com.ilhanli.omer.placesapp.module;

import com.ilhanli.omer.placesapp.service.FourSquareService;
import com.ilhanli.omer.placesapp.service.FourSquareServiceImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MyModule {

    /**
     * Dagger2 Container bize FourSquareService objesi üretir.
     */
    @Provides
    @Singleton
    static FourSquareService provideMyExample() {
        return new FourSquareServiceImpl();
    }

}
