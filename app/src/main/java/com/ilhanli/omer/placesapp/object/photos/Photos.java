package com.ilhanli.omer.placesapp.object.photos;

import com.ilhanli.omer.placesapp.object.explore.Photo;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

/**
 * Created by omerilhanli on 8.07.2018.
 */
@Generated("org.jsonschema2pojo")
public class Photos implements Serializable {

    private static final long serialVersionUID = -3461277465327225157L;
    private int count;

    private List<Photo> items;

    private int dupesRemoved;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Photo> getItems() {
        return items;
    }

    public void setItems(List<Photo> items) {
        this.items = items;
    }

    public int getDupesRemoved() {
        return dupesRemoved;
    }

    public void setDupesRemoved(int dupesRemoved) {
        this.dupesRemoved = dupesRemoved;
    }
}
