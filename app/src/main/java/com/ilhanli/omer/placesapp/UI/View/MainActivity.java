package com.ilhanli.omer.placesapp.UI.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.widget.Button;
import android.widget.EditText;

import com.ilhanli.omer.placesapp.UI.Contract.IMainView;
import com.ilhanli.omer.placesapp.UI.NormalBaseActivity;
import com.ilhanli.omer.placesapp.R;
import com.ilhanli.omer.placesapp.UI.Presenter.MainPresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends NormalBaseActivity implements IMainView.IView {

    @BindView(R.id.edittext_place)
    EditText edittextPlace;
    @BindView(R.id.card_view_place)
    CardView cardViewPlace;
    @BindView(R.id.edittext_city)
    EditText edittextCity;
    @BindView(R.id.card_view_city)
    CardView cardViewCity;
    @BindView(R.id.button_search)
    Button buttonSearch;
    @BindView(R.id.card_view_search)
    CardView cardViewSearch;

    private IMainView.IPresenter presenter;

    // Static olarak activity başlatılır
    public static void startActivity(Activity activity) {

        Intent intent = new Intent(activity, MainActivity.class);

        activity.startActivity(intent);

        activity.finish();
    }

    @Override
    protected int contentViewId() {

        return R.layout.activity_main;
    }

    @Override
    protected String getToolbarTitle() {

        return getString(R.string.title_bar_main);
    }

    @Override
    protected boolean getShowHomeButton() {

        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Activity, aslında view implementation'ıdır, ve Presenter'dan bir obje barındırı
        // Bu objeyi oluşturur ve pInit ile başlatır.
        presenter = new MainPresenter(this);

        presenter.pInit((MainPresenter) presenter);
    }

    // Her search işlemi için eventin düştüğü yerdir.
    @OnClick(R.id.button_search)
    public void eventSearch() {

        String place = edittextPlace.getText().toString();

        String city = edittextCity.getText().toString();

        presenter.pEventSearch(fourSquareService, place, city);
    }

    @Override
    public Context vGetContext() {
        return this;
    }

    @Override
    public void vShowProgress() {

        showDialog();
    }

    @Override
    public void vHideProgress() {

        dismissDialog();
    }
}
