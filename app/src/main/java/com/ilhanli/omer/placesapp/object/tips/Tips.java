package com.ilhanli.omer.placesapp.object.tips;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

/**
 * Created by omerilhanli on 8.07.2018.
 */

@Generated("org.jsonschema2pojo")
public class Tips implements Serializable {

    private static final long serialVersionUID = -4273586637587981301L;

    private int count;

    private List<Tip> items;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Tip> getItems() {
        return items;
    }

    public void setItems(List<Tip> items) {
        this.items = items;
    }
}

