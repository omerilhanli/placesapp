package com.ilhanli.omer.placesapp.UI.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ilhanli.omer.placesapp.R;
import com.ilhanli.omer.placesapp.UI.View.PlacesActivity;
import com.ilhanli.omer.placesapp.interfaces.OnAdapterListener;
import com.ilhanli.omer.placesapp.object.PlaceItem;
import com.ilhanli.omer.placesapp.service.FourSquareServiceImpl;
import com.ilhanli.omer.placesapp.utility.Util;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Servisten gelen tüm venue'ler adapter aracılığıyla listenir.
 *
 * Created by omerilhanli on 7.07.2018.
 */

public class PlacesRecyclerAdapter extends RecyclerView.Adapter<PlacesRecyclerAdapter.PlacesHolder> {

    @Inject
    FourSquareServiceImpl fourSquareService;

    private List<PlaceItem> placeList;

    private Context context;

    public PlacesRecyclerAdapter(Context context, List<PlaceItem> placeList) {

        this.placeList = placeList;

        this.context = context;
    }

    @Override
    public PlacesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place, parent, false);

        PlacesHolder placesHolder = new PlacesHolder(view);

        return placesHolder;
    }

    @Override
    public void onBindViewHolder(PlacesHolder holder, int position) {

        PlaceItem placeItem = placeList.get(position);

        holder.textViewplaceName.setText(placeItem.getVenue().getName());

        String placeAddress = Util.findAdressFull(holder.itemView.getContext(), placeItem.getVenue().getLocation());

        holder.textPlaceAddress.setText(placeAddress);

        holder.textPlaceCountry.setText(placeItem.getVenue().getLocation().getCountry().trim());


        holder.currentPosition = position;
    }

    @Override
    public int getItemCount() {

        return placeList.size();
    }

    public class PlacesHolder extends RecyclerView.ViewHolder {

        View itemView;

        @BindView(R.id.linear_layout_place)
        LinearLayout linearLayout;
        @BindView(R.id.text_view_place_name)
        TextView textViewplaceName;
        @BindView(R.id.text_place_address)
        TextView textPlaceAddress;
        @BindView(R.id.text_view_place_country)
        TextView textPlaceCountry;

        int currentPosition;

        public PlacesHolder(View itemView) {

            super(itemView);

            ButterKnife.bind(this, itemView);

            this.itemView = itemView;
        }

        // Listeden tıklanan her item PlaceActivity'ye gönderilir.
        @OnClick(R.id.linear_layout_place)
        public void eventClickPlace() {

            final PlaceItem placeItem = placeList.get(currentPosition);

            PlacesActivity activity = (PlacesActivity) context;

            OnAdapterListener adapterListener = activity;

            adapterListener.onTapItemListen(placeItem);
        }
    }
}
