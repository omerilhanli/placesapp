package com.ilhanli.omer.placesapp.UI.Model;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.ilhanli.omer.placesapp.UI.Contract.IMainView;
import com.ilhanli.omer.placesapp.interfaces.OnExploreComplete;
import com.ilhanli.omer.placesapp.object.PlaceItem;
import com.ilhanli.omer.placesapp.service.FourSquareService;
import com.ilhanli.omer.placesapp.utility.Util;

import java.util.List;

public class MainModel implements IMainView.IModel {

    private IMainView.IPresenterForModel presenter;

    private String mAddress = "";
    private String mQuery = "";

    public MainModel(IMainView.IPresenterForModel presenter) {

        this.presenter = presenter;
    }

    @Override
    public void mInit() {

        prepareLocation();
    }

    /**
     * Eğer Şehir veya Bölge boş bırakılmışsa, mevcut konum GPS aracılığıyla bulunur,
     * ve Search işlemi o sayede yapılır
     */
    private void prepareLocation() {

        final LocationManager locationManager = (LocationManager) presenter.pmGetContext().getSystemService(Context.LOCATION_SERVICE);

        // İlgili permissionlar sağlanmadıysa lokasyon search gerçekleşmez.
        if (ActivityCompat.checkSelfPermission(presenter.pmGetContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(presenter.pmGetContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }


        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                5000, 10, locationListener);
    }

    @Override
    public void mEventSearch(FourSquareService fourSquareService,final String place, String city) {

        if (city.isEmpty()) {

            // Şehir boş ve mekan arama alanında 3 karakterden az girildiyse uyarı verilir, Search gerçekleşmez!
            if (place.length() < 3) {

                Util.showPopup(presenter.pmGetContext(), "MEKAN UYARI", "Lütfen en az 3 karakterli bir MEKAN girin",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });

                return;

            } else {

                mQuery = place;
            }

        }

        if (!city.isEmpty()) {

            // Şehir boş değil ve şehir karakter sayısı 3 karakterden az ise uyarı verilir, Search gerçekleşmez!
            if (city.length() < 3) {

                Util.showPopup(presenter.pmGetContext(), "ŞEHİR UYARI", "Lütfen en az 3 karakterli bir ŞEHİR girin",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });


                return;

                // şehir normali ancak mekan karakter sayısı 3 karakterden az ise, uyarı verilir, search gerçekleşmez.
            } else if (place.length() < 3) {

                Util.showPopup(presenter.pmGetContext(), "MEKAN UYARI", "Lütfen en az 3 karakterli bir MEKAN girin",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });

                return;
            }

            // City ve Place, karakter sayısı valid ise, mAddress ve mQuery ayarlanır,
            if (place.length() >= 3) {

                mQuery = place;
            }

            if (place.length() >= 3) {

                mAddress = city;
            }
        }

        // Adres ve mekan bilgisi ile yakındaki tüm mekanlar için servise request atılır.
        fourSquareService.requestExplore(mAddress, mQuery, new OnExploreComplete() {

            @Override
            public void onExploreCompleted(List<PlaceItem> placeItemList) {

                // View güncellemesi için presenter notify edilir.
                presenter.pmFinished(placeItemList, mQuery);
            }

            @Override
            public void onVenuePhotosCompleted(String photoUrl) {

            }

            @Override
            public void onVenueTipsCompleted(String venueTip) {

            }
        });
    }


    LocationListener locationListener = new LocationListener() {

        public void onLocationChanged(Location location) {

            double longitude = location.getLongitude();

            double latitude = location.getLatitude();

            // Gps ile bulunan lat ve lng koordinat verilerinden gerçek adres bilgisi alınır.
            mAddress = Util.findAdressSummary(presenter.pmGetContext(), latitude, longitude);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };
}
