package com.ilhanli.omer.placesapp.interfaces;

import com.ilhanli.omer.placesapp.object.PlaceItem;

import java.util.List;

/**
 * Servisten yapılan request sonucunu aldığımız interface.
 */
public interface OnExploreComplete {

    void onExploreCompleted(List<PlaceItem> placeItemList);

    void onVenuePhotosCompleted(String photoUrl);

    void onVenueTipsCompleted(String venueTip);
}
