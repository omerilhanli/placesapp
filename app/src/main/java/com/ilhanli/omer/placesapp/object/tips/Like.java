package com.ilhanli.omer.placesapp.object.tips;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Generated;

/**
 * Created by omerilhanli on 8.07.2018.
 */

@Generated("org.jsonschema2pojo")
public class Like implements Serializable{

    private static final long serialVersionUID = -9030165844893229686L;

    private int count;

    private List<Object> groups;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Object> getGroups() {
        return groups;
    }

    public void setGroups(List<Object> groups) {
        this.groups = groups;
    }
}
