package com.ilhanli.omer.placesapp.service;

import com.ilhanli.omer.placesapp.object.photos.VenuePhoto;
import com.ilhanli.omer.placesapp.object.explore.Explore;
import com.ilhanli.omer.placesapp.object.tips.VenueTips;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Tüm foursquare requestlerimizin interface'i
 */

public interface FourSquareApi {

    // Creator(inner) helper classı ile FourSquareApi objesi alırız.
    class Creator {

        static <T> T create(final Class<T> service) {

            return new Retrofit.Builder()
                    .baseUrl("https://api.foursquare.com/v2/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(service);
        }
    }

    // Venue Id ile venue yorumu alınır
    @GET("venues/{VENUE_ID}/tips")
    Call<VenueTips> requestVenueTips(@Path("VENUE_ID") String venueId,
                                     @Query("client_id") String client_id,
                                     @Query("client_secret") String client_secret,
                                     @Query("v") String v,
                                     @Query("near") String near,
                                     @Query("query") String query);

    // Venue id ile venue resmi alınır
    @GET("venues/{VENUE_ID}/photos")
    Call<VenuePhoto> requestVenuePhoto(@Path("VENUE_ID") String venueId,
                                       @Query("client_id") String client_id,
                                       @Query("client_secret") String client_secret,
                                       @Query("v") String v,
                                       @Query("near") String near,
                                       @Query("query") String query);

    // istenen adreste, istenen mekana tipine göre tüm mekanlar alınır. (Mesela Kadıköydeki 'bar' lar.)
    @GET("venues/explore/")
    Call<Explore> requestExplore(
            @Query("client_id") String client_id,
            @Query("client_secret") String client_secret,
            @Query("v") String v,
            @Query("near") String near,
            @Query("query") String query);

}
